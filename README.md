# "Dirigiendo el cambio y la innovación en las TIC".

Lecturas de la asignatura: "Dirigiendo el cambio y la innovación en las TIC", de la 
Maestría en "Gestión de Innovación de las Tecnologías de Información y Comunicación", de INFOTEC (Mayo 2019).

**Tome en cuenta de que estos contenidos estarán completos a finales de julio de 2019**.

## Contenido.

Este repositorio contiene las versiones más actualizadas tanto de las lecturas como los recursos utilizados en la asignatura. 

Tanto las presentaciones como los documentos extendidos de las lecturas están en formato HTML.

### Presentaciones y lecturas.

* Las presentaciones de la asignatura utilizan el framework [Reveal.js](https://revealjs.com) y están localizadas en el directorio [presentaciones/](presentaciones/).
* Las lecturas completas de la asignatura están localizadas en el directorio [lecturas/](lecturas/).

### Código fuente de los documentos.

Las fuentes de las lecturas son notebooks de [Jupyter](https://jupyter.org) y están localizadas.

* [presentaciones/fuentes/](presentaciones/fuentes/).
* [lecturas/fuentes/](lecturas/fuentes/).

## Descarga del repositorio.

Para descargar el contenido de este repositorio ejecute el siguiente comando:

```git clone https://github.com/josechval/cambio_e_innovacion_tic.git```